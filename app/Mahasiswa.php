<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
  // DEFINITION SECTION ------------------------------------------------------
  // Nama Table
  protected $table = 'table_mahasiswa';
  
  // setup index dan primary key
  protected $guarded = ['id'];
  protected $primaryKey = 'id';
  
  // RELATION SECTION ---------------------------------------------------------
  // Relasi terhadap Jurusan
  function Jurusan(){
    // menyatakan bahwa idJurusan dimiliki oleh Model Jurusan
  	return $this->belongsTo('App\Jurusan','idJurusan');
  }
  
  // Relasi terhadap Staff
  function Staff(){
    // menyatakan bahwa idStaff dimiliki oleh Model Staff
  	return $this->belongsTo('App\Staff','idStaff');
  }
}
