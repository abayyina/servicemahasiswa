<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    //
	protected $table = 'table_staff';
	
	protected $guarded = ['id'];
	protected $primaryKey = 'id';
	
}
