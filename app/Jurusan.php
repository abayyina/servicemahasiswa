<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    //
	protected $table = 'table_jurusan';
	
	protected $guarded = ['id'];
	protected $primaryKey = 'id';
	
	
	function Mahasiswa(){
		return $this->belongsTo('App\Mahasiswa','idMahasiswa');
	}
}
