<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mahasiswa;
use App\Jurusan;
use App\Staff;

class Webtools extends Controller
{
  public function setupResponse($jenis = null){
    return array(
      "success" => 0,
      "message" => "Error happens",
      "data" => array()
    );
  }
 
  // -- Get mahasiswa
  public function ambilMahasiswa(){
    // initialize response
    $res = $this->setupResponse("fetch");
    
    // initailize model 
    $mahasiswa = new Mahasiswa;
    
    // get mahasiswa
    $res['data'] = $mahasiswa->with([
		'Staff' => function($q){
			$q->select('id','nama');
		},
		'Jurusan' => function($q){
			$q->select('id','nama');
		}
		])->get();
    
    // setup response success
    if($res['data']){
    	$res['success'] = 1; 
    	$res['message'] = "Mengambil data mahasiswa ";
    }else{
    	$res['message'] = "Gagal mengambil data mahasiswa ";
    }
    
		return json_encode($res);
  }
}
