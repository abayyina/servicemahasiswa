<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Rute mahasiswa 
// --Tambah mahasiswa baru (POST)
Route::post('/mahasiswa/','Webtools@newMahasiswa');
// --Tampilkan mahasiswa (GET)
Route::get('/mahasiswa/','Webtools@ambilMahasiswa');
// --Hapus mahasiswa (DELETE)
Route::delete('/mahasiswa/','Webtools@hapusMahasiswa');
// --Update Mahasisiswa (PUT)
Route::put('/mahasiswa/','Webtools@updateMahasiswa');

// Jurusan
// --Tambah Jurusan
Route::post('/jurusan/','Jurusan@newJurusan');
// --Tampilkan Jurusan
Route::get('/jurusan/','Jurusan@ambilJurusan');
// --Hapus Jurusan
Route::delete('/jurusan/','Jurusan@hapusJurusan');
// --Update Jurusan
Route::put('/jurusan/','Jurusan@updateJurusan');